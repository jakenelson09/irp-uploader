import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import UploadFiles from "./components/upload-files.component";

function App() {
  return (
    <div className="container" style={{ width: "600px" }}>
      <div style={{ margin: "40px 10px 10px 10px" }}>
        <h3>OHSU Image Repository Upload</h3>
        <br/>
      </div>
      <UploadFiles />
    </div>
  );
}

export default App;
